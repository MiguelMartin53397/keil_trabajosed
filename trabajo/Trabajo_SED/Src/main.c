/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;

volatile int modo;                            					//variable que asigna el modo en el que nos encontramos.
volatile int nma;                  											//variables de control modo vacaciones, nada mas activar.


/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM1_Init2(void);
static void MX_TIM2_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM4_Init2(void);


void modoNormal(int *es1,int *es2,int *es3,int *ec1,int *ec2,int *ec3);
void comprobarPresencia(int *e1,int *e2,int *e3);
void comprobarPresenciaC(int *e1,int *e2,int *e3);
void salon(int *es1,int *es2,int *es3);
int valorCAD(void);
void cocina(int *ec1,int *ec2,int *ec3);
void pasillo(void);
void apagarTodo(int *es1,int *es2,int *es3,int *ec1,int *ec2,int *ec3);
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
void modoVacaciones(int *luzCS,int *ant);
void modoPresencia(int *luzSalonEncendida);


/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	
				/* variables de control salon */
	int estadoS1;                  								//detectando presencia
	int estadoS2;																	//temp canal 1____luz activa
	int estadoS3;																	// temp canal 2___volviendo detectar presencia
	
				/* variables de control cocina */
	int estadoC1;                  								//detectando presencia
	int estadoC2;																	//temp canal 1____luz activa
	int estadoC3;																	// temp canal 2___volviendo detectar presencia
	
					/* variables de control modo vacaciones */
	int luzCS;															//que luz se enciende
	int ant;																//volor de timer en t-1
	
						/* variables de control modo interruptore */
	int luzSalonEncendida;
	
  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_ADC1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  /* USER CODE BEGIN 2 */
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,0);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_13,0);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_14,0);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_15,0);
	HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,0);
	
	modo = 0;
	nma = 0;
	luzCS = 0;
	ant = 1;
	luzSalonEncendida = 0;
	
	estadoS1 = 0;
	estadoS2 = 0;
	estadoS3 = 0;
	
	HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_3);
	
	estadoC1 = 0;
	estadoC2 = 0;
	estadoC3 = 0;
	
	HAL_TIM_OC_Start(&htim2,TIM_CHANNEL_3);
	
  /* USER CODE END 2 */
 

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */
		if( modo == 0 )
		{
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,1);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_13,0);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_14,0);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_15,0);
			modoNormal(&estadoS1,&estadoS2,&estadoS3,&estadoC1,&estadoC2,&estadoC3);

		}
		
		if( modo == 1)
		{
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,0);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_13,1);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_14,0);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_15,0);
			apagarTodo(&estadoS1,&estadoS2,&estadoS3,&estadoC1,&estadoC2,&estadoC3);
			HAL_Delay(1000);
			modo = 0;
		}
		
		if( modo == 2 )
		{
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,0);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_13,0);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_14,1);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_15,0);
			if(nma)
			{
				apagarTodo(&estadoS1,&estadoS2,&estadoS3,&estadoC1,&estadoC2,&estadoC3);
				nma = 0;
			}
			modoVacaciones(&luzCS,&ant);
		}
		
		if( modo == 3 )
		{
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,0);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_13,0);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_14,0);
			HAL_GPIO_WritePin(GPIOD,GPIO_PIN_15,1);
			if(nma)
			{
				apagarTodo(&estadoS1,&estadoS2,&estadoS3,&estadoC1,&estadoC2,&estadoC3);
				nma = 0;
			}
			modoPresencia(&luzSalonEncendida);
		}
		
		if( modo > 3 )
			modo = 0;
		
		if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_12))
			modo = 0;
		if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_13))
			modo = 1;
		if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_14)){
			modo = 2; nma= 1;}
		if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_15)){
			modo = 3; nma = 1;}
		
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}




void modoNormal(int *es1,int *es2,int *es3,int *ec1,int *ec2,int *ec3)
{
	salon(es1,es2,es3);
	cocina(ec1,ec2,ec3);
	pasillo();
}

void salon(int *es1,int *es2,int *es3)
{
	comprobarPresencia(es1,es2,es3);
	
	if(*es1 == 1 || *es2 == 1 || *es3 == 1)
		htim5.Instance ->CCR3 = valorCAD();
	
	else
		htim5.Instance ->CCR3 = 0;
}

void cocina(int *ec1,int *ec2,int *ec3)
{
	comprobarPresenciaC(ec1,ec2,ec3);
	
	if(*ec1 == 1 || *ec2 == 1 || *ec3 == 1)
		HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,1);
	
	else
		HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,0);
}

void pasillo(void)
{
	if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1))
		HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,1);
	else
		HAL_GPIO_WritePin(GPIOB,GPIO_PIN_5,0);
}

void comprobarPresencia(int *e1, int *e2,int *e3)
{
	if( *e1 == 0 && *e2 == 0 && *e3 == 0)
	{
		if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_0))
		{
			*e1 = 0;
			*e2 = 1;
			*e3 = 0;
			
			MX_TIM4_Init();
			HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_1);
			HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_2);
		}
	}
	
	if( *e1 == 0 && *e2 == 1 && *e3 == 0)
	{
		if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_7))
		{
			*e1 = 0;
			*e2 = 0;
			*e3 = 1;
		}
	}
	
	if( *e1 == 0 && *e2 == 0 && *e3 == 1)
	{
		if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_0))
		{
			*e1 = 0;
			*e2 = 1;
			*e3 = 0;
			
			HAL_TIM_OC_Stop(&htim4,TIM_CHANNEL_1);
			HAL_TIM_OC_Stop(&htim4,TIM_CHANNEL_2);
			MX_TIM4_Init2();
			HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_1);
			HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_2);
			HAL_Delay(1500);
			HAL_TIM_OC_Stop(&htim4,TIM_CHANNEL_1);
			HAL_TIM_OC_Stop(&htim4,TIM_CHANNEL_2);
			MX_TIM1_Init();
			HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_1);
			HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_2);
		}
		if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_6) == 0)
		{
			*e1 = 0;
			*e2 = 0;
			*e3 = 0;
			
			HAL_TIM_OC_Stop(&htim4,TIM_CHANNEL_1);
			HAL_TIM_OC_Stop(&htim4,TIM_CHANNEL_2);
			MX_TIM4_Init2();
			HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_1);
			HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_2);
			HAL_Delay(1500);
			HAL_TIM_OC_Stop(&htim4,TIM_CHANNEL_1);
			HAL_TIM_OC_Stop(&htim4,TIM_CHANNEL_2);
		}
	}
	
}

int valorCAD(void)
{
	int resultado;
	HAL_ADC_Start(&hadc1);
	if(HAL_ADC_PollForConversion(&hadc1,5)==HAL_OK)
	{
		resultado = HAL_ADC_GetValue(&hadc1);
	}
	HAL_ADC_Stop(&hadc1);
	resultado = (100 * resultado) / 255;
	return resultado;
}


void comprobarPresenciaC(int *e1,int *e2,int *e3)
{
		if( *e1 == 0 && *e2 == 0 && *e3 == 0)
	{
		if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_2))
		{
			*e1 = 0;
			*e2 = 1;
			*e3 = 0;
			
			MX_TIM1_Init();
			HAL_TIM_OC_Start(&htim1,TIM_CHANNEL_1);
			HAL_TIM_OC_Start(&htim1,TIM_CHANNEL_2);
		}
	}
	
	if( *e1 == 0 && *e2 == 1 && *e3 == 0)
	{
		if(HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_11))
		{
			*e1 = 0;
			*e2 = 0;
			*e3 = 1;
		}
	}
	
	if( *e1 == 0 && *e2 == 0 && *e3 == 1)
	{
		if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_2))
		{
			*e1 = 0;
			*e2 = 1;
			*e3 = 0;
			
			HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_1);
			HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_2);
			MX_TIM1_Init2();
			HAL_TIM_OC_Start(&htim1,TIM_CHANNEL_1);
			HAL_TIM_OC_Start(&htim1,TIM_CHANNEL_2);
			HAL_Delay(1500);
			HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_1);
			HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_2);
			MX_TIM1_Init();
			HAL_TIM_OC_Start(&htim1,TIM_CHANNEL_1);
			HAL_TIM_OC_Start(&htim1,TIM_CHANNEL_2);
		}
		if(HAL_GPIO_ReadPin(GPIOE,GPIO_PIN_9))
		{
			*e1 = 0;
			*e2 = 0;
			*e3 = 0;
			
			HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_1);
			HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_2);
			MX_TIM1_Init2();
			HAL_TIM_OC_Start(&htim1,TIM_CHANNEL_1);
			HAL_TIM_OC_Start(&htim1,TIM_CHANNEL_2);
			HAL_Delay(1500);
			HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_1);
			HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_2);
		}
	}
}

void apagarTodo(int *es1,int *es2,int *es3,int *ec1,int *ec2,int *ec3)
{
	*es1 = 0;
	*es2 = 0;
	*es3 = 0;
	*ec1 = 0;
	*ec2 = 0;
	*ec3 = 0;
	
	HAL_TIM_OC_Stop(&htim4,TIM_CHANNEL_1);
	HAL_TIM_OC_Stop(&htim4,TIM_CHANNEL_2);
	MX_TIM4_Init2();
	HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_1);
	HAL_TIM_OC_Start(&htim4,TIM_CHANNEL_2);
	HAL_Delay(1500);
	HAL_TIM_OC_Stop(&htim4,TIM_CHANNEL_1);
	HAL_TIM_OC_Stop(&htim4,TIM_CHANNEL_2);
	
	HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_1);
	HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_2);
	MX_TIM1_Init2();
	HAL_TIM_OC_Start(&htim1,TIM_CHANNEL_1);
	HAL_TIM_OC_Start(&htim1,TIM_CHANNEL_2);
	HAL_Delay(1500);
	HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_1);
	HAL_TIM_OC_Stop(&htim1,TIM_CHANNEL_2);
	
}


void modoVacaciones(int *luzCS,int *ant)
{
	if(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_10) != *ant)
		(*luzCS)++;
	if(*luzCS > 1)
		*luzCS = 0;
	if(*luzCS == 0)
		{
			HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,1);
			htim5.Instance ->CCR3 = 0;
		}
	if(*luzCS == 1)
		{
			HAL_GPIO_WritePin(GPIOB,GPIO_PIN_4,0);
			htim5.Instance ->CCR3 = 65;
		}
		*ant = *luzCS;
}

void modoPresencia(int *luzSalonEncendida)
{
	if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_0) == 1)
		(*luzSalonEncendida)++;
	if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_2) == 1)
		HAL_GPIO_TogglePin(GPIOB,GPIO_PIN_4);
	if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_1) == 1)
		HAL_GPIO_TogglePin(GPIOB,GPIO_PIN_5);
	if(*luzSalonEncendida == 1)
		htim5.Instance ->CCR3 = 100;
	if(*luzSalonEncendida == 0)
		htim5.Instance ->CCR3 = 0;
	if(*luzSalonEncendida > 1)
		*luzSalonEncendida = 0;
}









/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_8B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 16000;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 20000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
  sConfigOC.Pulse = 20000;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_OC_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.Pulse = 10000;
  if (HAL_TIM_OC_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

static void MX_TIM1_Init2(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 16000;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 1000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_FORCED_INACTIVE;
  sConfigOC.Pulse = 1000;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_OC_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.Pulse = 1000;
  if (HAL_TIM_OC_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 16000;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 15000;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
  sConfigOC.Pulse = 15000;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 16000;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 20000;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TOGGLE;
  sConfigOC.Pulse = 20000;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.Pulse = 10000;
  if (HAL_TIM_OC_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

static void MX_TIM4_Init2(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 16000;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 1000;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_FORCED_INACTIVE;
  sConfigOC.Pulse = 1000;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.Pulse = 1000;
  if (HAL_TIM_OC_ConfigChannel(&htim4, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */
  HAL_TIM_MspPostInit(&htim4);

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 16000;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 10;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 50;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim5, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  /* USER CODE END TIM5_Init 2 */
  HAL_TIM_MspPostInit(&htim5);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4|GPIO_PIN_5, GPIO_PIN_RESET);

  /*Configure GPIO pin : PC15 PC14 PC13 PC12 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PC0 PC1 PC2 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PD12 PD13 PD14 PD15 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : PB4 PB5 */
  GPIO_InitStruct.Pin = GPIO_PIN_4|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
